# initial

ROOT= build
PROJ= demo

BDIR= $(ROOT)

CFLAGS	+= -Wall -Werror -pedantic

CONFIG 	= config
OPTLVL  = $(ROOT)/optlvl.cfg
TARGET  = $(ROOT)/target.cfg

-include $(TARGET)
-include $(OPTLVL)

SRCS= $(wildcard src/*.c) main.c
MODS= $(patsubst src/%,%, $(SRCS))
OBJS= $(patsubst %.c,$(BDIR)/%.o, $(MODS))

.PHONY: clean run release debug linux windows web

all: |$(BDIR) check_optlvl check_target $(PROJ)

# it requires WGPUBIN WGPUTAR

WGPUURL := https://github.com/gfx-rs/wgpu-native.git
WGPUDIR	:= $(ROOT)/wgpu-native
WGPU	:= $(BDIR)/$(WGPUBIN)

CFLAGS	+= -I$(WGPUDIR)/ffi/webgpu-headers/

$(WGPUDIR):
	git clone \
		--depth=1 \
		--recurse-submodules=ffi/webgpu-headers \
		$(WGPUURL) \
		$(WGPUDIR)

$(WGPU): |$(WGPUDIR)
	make -C $(WGPUDIR) $(WGPUTAR)
	ln -fsr $(WGPUDIR)/target/$(OPTMODE)/$(WGPUBIN) $(WGPU)

$(PROJ): $(OBJS) $(WGPU)
	$(CC) -o $(PROJ) $(OBJS) $(WGPU) $(LDFLAGS) $(CFLAGS)
$(OBJS): $(SRCS)
	$(CC) -c $< -o $@ $(CFLAGS)

release:|$(BDIR)
	ln -fsr $(CONFIG)/release.cfg   $(OPTLVL)
debug:	|$(BDIR)
	ln -fsr $(CONFIG)/debug.cfg     $(OPTLVL)
check_optlvl:
ifneq ($(wildcard $(OPTLVL)),$(OPTLVL))
	$(error Set optimization level via make release/debug)
endif

linux:	|$(BDIR)
	ln -fsr $(CONFIG)/linux.cfg     $(TARGET)
windows:|$(BDIR)
	ln -fsr $(CONFIG)/windows.cfg   $(TARGET)
web:	|$(BDIR)
	ln -fsr $(CONFIG)/web.cfg       $(TARGET)
check_target:
ifneq ($(wildcard $(TARGET)),$(TARGET))
	$(error Set platform via make linux/windows/web)
endif

$(BDIR):
	mkdir -p $(BDIR)

clean:
	rm -rf $(ROOT)

run: all
	./$(PROJ)