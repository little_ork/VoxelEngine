#include "SDL_WGPUSurface.h"
#include <SDL_syswm.h>
#include <assert.h>

WGPUSurface SDL_CreateWGPUSurface( SDL_Window *w, WGPUInstance inst ) {
    assert( w && inst );

    SDL_SysWMinfo info = {0};
    SDL_VERSION( &info.version );
    if( !SDL_GetWindowWMInfo( w, &info ))
        return NULL;

#ifdef LINUX
    switch( info.subsystem ) {

    case SDL_SYSWM_X11: {

        WGPUSurfaceDescriptorFromXlibWindow platform_spec = {
            .display = info.info.x11.display,
            .window = info.info.x11.window,
            .chain.sType = WGPUSType_SurfaceDescriptorFromXlibWindow,
            .chain.next = NULL,
        };

        WGPUSurfaceDescriptor surface_desc = {
            .nextInChain = &platform_spec.chain, 0
        };

        return wgpuInstanceCreateSurface( inst, &surface_desc );

    } break;

    case SDL_SYSWM_WAYLAND: {

         WGPUSurfaceDescriptorFromWaylandSurface platform_spec = {
            .display = info.info.wl.display,
            .surface = info.info.wl.surface,
            .chain.sType = WGPUSType_SurfaceDescriptorFromWaylandSurface,
            .chain.next = NULL,
        };

        WGPUSurfaceDescriptor surface_desc = {
            .nextInChain = &platform_spec.chain, 0
        };

        return wgpuInstanceCreateSurface( inst, &surface_desc );

    } break;

    default: 
        return SDL_Unsupported(), NULL;

    };
#endif//LINUX

#ifdef WINDOWS
    WGPUSurfaceDescriptorFromWindowsHWND platform_spec = {
        .hinstance = info.info.win.hinstance,
        .hwnd = info.info.win.window,
        .chain.sType = WGPUSType_SurfaceDescriptorFromWindowsHWND,
        .chain.next = NULL,
    };

    WGPUSurfaceDescriptor surface_desc = {
        .nextInChain = &platform_spec.chain, 0
    };

    return wgpuInstanceCreateSurface( inst, &surface_desc );
#endif//WINDOWS

#ifdef WEB
    // ...
#endif//WEB
    
    return SDL_Unsupported(), NULL;
}
