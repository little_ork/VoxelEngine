#ifndef SDL_WGPUSURFACE_H
#define SDL_WGPUSURFACE_H

#include <SDL.h>
#include <webgpu.h>

WGPUSurface SDL_CreateWGPUSurface( SDL_Window *w, WGPUInstance inst );

#endif//SDL_WGPUSURFACE_H
